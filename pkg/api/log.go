package api

// Logging field names. Are here for consistency across codebase.
const (
	ProjectId = "project_id"
	ClusterId = "cluster_id"
	AgentId   = "agent_id"

	ResourceKey = "resource_key"
	SyncResult  = "sync_result"
)
