Code in this package and subpackages was copied from Workhorse. Because `kgb` may become part of Workhorse, it does not make sense to
invest any effort into moving this code out of Workhorse to make it reusable right now. If `kgb` stays a separate component
we'd want to reuse code properly and put it somewhere else, maybe labkit.
