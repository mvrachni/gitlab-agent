// Mocks for gitops-engine.
package mock_engine

//go:generate go run github.com/golang/mock/mockgen -destination "engine.go" "github.com/argoproj/gitops-engine/pkg/engine" "GitOpsEngine"
